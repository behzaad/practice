#include<iostream>
using namespace std;
class cls{
private:
  double img,re;
public:
  cls(double img=0,double re=0){
    this->img=img;
    this->re=re;
    cout<<"constructor"<<' '<<img<<' '<<re<<endl;
  }
  cls(cls &a){
    this->img=a.img;
    this->re=a.re;
    cout<<"copy"<<endl;
  }
  ~cls(){
    cout<<"destructor"<<img<<' '<<re<<endl;
  }

  /// if you call the operator that is friend you need the object in it but if you call the operator that is in the class you don t need it like operator in line 20

  friend ostream& operator <<(ostream &,const cls &);  ///the << >> operators must be friend
  friend istream& operator >>(istream &,cls &);
  /*cls& operator+(const cls &a){
    img=img+a.img;
    re=re+a.re;
    return *this;      ///if the operator is in the class you can return *this but if your operator is out of class you can not do that
  }*/
  friend cls& operator+(cls &,const cls &);
  friend cls & operator+(const int &,cls &);
};

ostream& operator <<(ostream & co,const cls & a){
  co<<a.img<<' '<<a.re<<endl;
  return co;
}
istream& operator >>(istream & in,cls & a){
  in>>a.img>>a.re;
  return in;
}
cls& operator+(cls &a,const cls &b){
  a.img=a.img+b.img;
  a.re=a.re+b.re;
  return a;
}

cls & operator+(const int &b,cls &a){
  a.img=a.img+b;
  return a;
}

int main(){
  cls d,a;

/*  cls *p;     ///constructor call
  p=new cls(1,2);     ///no constructor
  delete p;

  cls *p;
  p=new cls[20]   ///array form pointers for object
  or
  p=new cls[20]={cls(1,5),cls(2,5)};

  p[0]
  p[1]
  ...

  //cin>>*p;
  //cout<<*p;       ///destructor call

  p->f();      ///for call function every thing with pointer we use ->
  p->img=5;

  Cls* f2(void){
    complexCls *pc;
    pc=new Cls(2,4);
    //delete pc;
    return pc;
  }
  void f3(Cls *p1){
    delete p1;
  }
  f2(f3());
*/


/*
  //b=5+b;
  //b=b+5;    ///for this + the constructor and destructor call  why??
  //b=b+a;
  d=a+5+6+7;   ///three constructor and destructor call for this +
  d=5+6+7;     ///for this just one constructor call
*/
}
