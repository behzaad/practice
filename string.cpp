#include<iostream>
using namespace std;

class mystring{
private:
  char s[100];
public:
  mystring(const char *a=" "){
    int i=0;
    for(;a[i];i++){
      s[i]=a[i];
    }
    s[i]=0;
    cout<<"constructor"<<endl;
  }
  mystring(mystring &a){
    int i=0;
    for(;a.s[i];i++){
      s[i]=a.s[i];
    }
    s[i]=0;
    cout<<"copy"<<endl;
  }
  ~mystring(){
    cout<<"destructor"<<endl;
  }
  friend ostream& operator<<(ostream &,const mystring &);
  friend istream& operator>>(istream &,mystring &);
/*
  mystring& operator+(const mystring &a){
    int i=0,j=0;
    for(;s[i];i++);
    for(;a.s[j];i++,j++){
      s[i]=a.s[j];
    }
    s[i]=0;
    return*this;
  }
*/
  friend mystring& operator+(mystring &,const mystring&);
  bool operator<(const mystring &a){
    int i=0;
    for(;a.s[i] && s[i];i++){
      if(a.s[i] > s[i]) return true;
      if(a.s[i] < s[i]) return false;
    }
      return 1;
  }
  bool operator >(const mystring &a){
    int i=0;
    for(;a.s[i] && s[i];i++){
      if(a.s[i] > s[i]) return false;
      if(a.s[i] < s[i]) return true;
    }
      return 0;
  }
  char& operator [](const int &i){
    return s[i];
  }

};
  ostream& operator<<(ostream &co,const mystring &a){
    int i;
    for(i=0;a.s[i];i++){
      co<<a.s[i];
    }
    co<<endl;
    return co;
  }
  istream& operator>>(istream &in,mystring &a){
      in>>a.s;
    return in;
  }
  mystring& operator+(mystring &a,const mystring&b){
    int i=0,j=0;
    for(;a.s[i];i++);
    for(;b.s[j];i++,j++){
      a.s[i]=b.s[j];
    }
    a.s[i]=0;
    return a;
  }
  int main(){
  }
